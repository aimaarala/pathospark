package org.ngseq.pathospark

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, FileSystem, Path}
import org.apache.spark.sql.SparkSession
import scala.collection.mutable.ArrayBuffer
import scala.sys.process._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.max
import util.control.Breaks._

object StrainDetect {

    def main(args: Array[String]) {

        val mlstin = args(0)

        val spark = SparkSession.builder.appName("StrainDetect").master("local").getOrCreate()

        val sample = "C24"
        val threshold_aln = 1
        import spark.implicits._

        val rawdata = spark.read.option("sep","\t").option("inferSchema", "true").csv("cfsan_mapped/"+sample+".sam")
        val mapped = rawdata.withColumn("_c13", regexp_replace(col("_c13"), "AS:i:", "")).withColumn("_c13", '_c13.cast("Int"))

        val mlst = spark.read.option("sep","\t").option("inferSchema", "true").option("header","true").csv(mlstin)
        val max_score =mapped.select(max("_c13")).head.get(0).toString.toInt
        var scored = mapped.filter(v => v.get(13).toString.toInt > (max_score-threshold_aln.toInt))

        var t = threshold_aln.toInt*2
        breakable {
            while(scored.count<10){
                scored = mapped.filter(v => v.get(13).toString.toInt > (max_score-t))
                t=t*2
                println(t)
                if(t>(max_score))
                    break
            }
        }

        //scored = scored.filter(v=>v.get(2).toString!="sucA_584")
        scored.show

        val columns = mlst.columns

        var filtered = mlst
        var temp = mlst

        var matched = ArrayBuffer.empty[Array[String]]
        val scoreddf = scored.select("_c2").withColumn("name",split(col("_c2"),"_")(0)).withColumn("id",split(col("_c2"),"_")(1))

        scoreddf.createOrReplaceTempView("scored")

        val mlstarr = mlst.collect()
        val scorearr = scoreddf.collect()
        var maxs=0
        var maxst = Seq(0,0,0,0,0,0,0)
        mlstarr.foreach(st => {

            var score = 0
            var a,d,hem,his,p,s,t = 0
            scorearr.foreach(v=>{
                if(v.get(1).toString=="aroC" && v.get(2).toString.toInt==st.get(1).toString.toInt){
                    a=st.get(1).toString.toInt
                    score+=1
                }
                if(v.get(1).toString=="dnaN" && v.get(2).toString.toInt==st.get(2).toString.toInt){
                    d=st.get(2).toString.toInt
                    score+=1
                }
                if(v.get(1).toString=="hemD" && v.get(2).toString.toInt==st.get(3).toString.toInt){
                    hem=st.get(3).toString.toInt
                    score+=1
                }
                if(v.get(1).toString=="hisD" && v.get(2).toString.toInt==st.get(4).toString.toInt){
                    his=st.get(4).toString.toInt
                    score+=1
                }
                if(v.get(1).toString=="purE" && v.get(2).toString.toInt==st.get(5).toString.toInt){
                    p=st.get(5).toString.toInt
                    score+=1
                }
                if(v.get(1).toString=="sucA" && v.get(2).toString.toInt==st.get(6).toString.toInt){
                    s=st.get(6).toString.toInt
                    score+=1
                }
                if(v.get(1).toString=="thrA" && v.get(2).toString.toInt==st.get(7).toString.toInt){
                    t=st.get(7).toString.toInt
                    score+=1
                }

            })

            if(score>maxs){
                maxs=score
                maxst = Seq(a,d,hem,his,p,s,t)
            }
        })

        val fasta = scored.map(sam=>{
            val name = ">"+sam.get(0).toString+"_"+sam.get(2).toString
            val seq = sam.get(9).toString
            name+"\n"+seq
        })


        //ST|aroC|dnaN|hemD|hisD|purE|sucA|thrA
        var i = 0
        var where = ""
        var c = 0
        maxst.foreach(v=>
            if(v!=0) c+=1
        )
        var c2=0
        for (id <- maxst) {
            i = i + 1
            var gene = ""
            if (id != 0) {
                c2+=1
                where += columns(i) + "=" + id
                if(c2<c)
                    where += " and "
            }
        }
        mlst.createOrReplaceTempView("mlst")

        val aln_results = spark.sql("select * from mlst where "+where)

        scored.show
        maxst
        aln_results.show

        val blast = spark.read.option("sep","\t").option("inferSchema", "true").csv("cfsan_blast/"+sample)

        val blasteddf = blast.orderBy(asc("_c10")).limit(100).groupBy("_c1").agg(max("_c11") as "max").select("_c1","max").withColumn("name",split(col("_c1"),"_")(0)).withColumn("id",split(col("_c1"),"_")(1))
        blasteddf.createOrReplaceTempView("blasted")

        val blastarr = blasteddf.collect()
        var bmaxs=0
        var bmaxst = Seq(0,0,0,0,0,0,0)
        mlstarr.foreach(st => {

            var score = 0
            var a,d,hem,his,p,s,t = 0
            blastarr.foreach(v=>{
                if(v.get(2).toString=="aroC" && v.get(3).toString.toInt==st.get(1).toString.toInt){
                    a=st.get(1).toString.toInt
                    score+=1
                }
                if(v.get(2).toString=="dnaN" && v.get(3).toString.toInt==st.get(2).toString.toInt){
                    d=st.get(2).toString.toInt
                    score+=1
                }
                if(v.get(2).toString=="hemD" && v.get(3).toString.toInt==st.get(3).toString.toInt){
                    hem=st.get(3).toString.toInt
                    score+=1
                }
                if(v.get(2).toString=="hisD" && v.get(3).toString.toInt==st.get(4).toString.toInt){
                    his=st.get(4).toString.toInt
                    score+=1
                }
                if(v.get(2).toString=="purE" && v.get(3).toString.toInt==st.get(5).toString.toInt){
                    p=st.get(5).toString.toInt
                    score+=1
                }
                if(v.get(2).toString=="sucA" && v.get(3).toString.toInt==st.get(6).toString.toInt){
                    s=st.get(6).toString.toInt
                    score+=1
                }
                if(v.get(2).toString=="thrA" && v.get(3).toString.toInt==st.get(7).toString.toInt){
                    t=st.get(7).toString.toInt
                    score+=1
                }

            })

            if(score>bmaxs){
                bmaxs=score
                bmaxst = Seq(a,d,hem,his,p,s,t)
            }
        })

        var i2 = 0
        var where2 = ""
        var b = 0
        bmaxst.foreach(v=>
            if(v!=0) b+=1
        )
        var b2=0
        for (id <- bmaxst) {
            i2 = i2 + 1
            var gene = ""
            if (id != 0) {
                b2+=1
                where2 += columns(i2) + "=" + id
                if(b2<b)
                    where2 += " and "
            }
        }

        val blast_results = spark.sql("select * from mlst where "+where2)

        scored.show
        maxst
        aln_results.show

        bmaxst
        blast_results.show

        blast_results.write.option("header","true").csv("cfsan_results/"+sample+"_blastpredict")
        blasteddf.write.option("header","true").csv("cfsan_results/"+sample+"_blast_best")

        fasta.write.text("cfsan_results/"+sample+"_scored_sam")
        aln_results.write.option("header","true").csv("cfsan_results/"+sample+"_mappredict")
        Seq(maxst.mkString(" ")).toDF.write.text("cfsan_results/"+sample+"_maxst")


        blast.filter(v=>(v.get(1).toString=="aroC_41"||v.get(1).toString=="dnaN_92"||v.get(1).toString=="hemD_84"||v.get(1).toString=="hisD_97"||v.get(1).toString=="purE_98"||v.get(1).toString=="sucA_104"||v.get(1).toString=="thrA_98")).show(false)

    }
}