package org.ngseq.pathospark

import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object Abundance {
    def main(args: Array[String]) = {

        Logger.getLogger("org").setLevel(Level.OFF)
        Logger.getLogger("akka").setLevel(Level.OFF)

        val spark = SparkSession.builder.master("local[*]").appName("main").config("spark.driver.memory", "5g").getOrCreate()

        import spark.implicits._

        val schemaString = "_c2 qty"

        // Generate the schema based on the string of schema
        val fields = schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, nullable = true))
        val schema = StructType(fields)

        var newNames = Seq("ref_id","c1")

        var qt = spark.read.option("sep", ",").schema(schema).option("inferSchema", "true").csv("qty1")
        for (i <- 2 to 21) {
            var fn = ""
            if(i>9){
                fn= "C" + i + "mapped.sam"
            }
            else {
                fn= "C0" + i + "mapped.sam"
            }
            val df = spark.read.option("sep", "\t").option("inferSchema", "true").csv("mapped/"+fn).drop(col("_c15")).withColumn("id", lit(1))
            val mqgt30 = df.filter(mq => mq.get(4).toString.toInt > 30)
            //val asf = mqgt30.withColumn("_c13", regexp_replace(col("_c13"), "AS:i:", "")).withColumn("_c13", '_c13.cast("Int"))
            //val as_filtered = asf.filter(v => v.get(13).toString.toInt > 50)
            //val totalc1 = as_filtered.count
            val totalc1 = mqgt30.count
            val mapped_to_ref = mqgt30.select("*").groupBy("_c2").count
            val qty = mapped_to_ref.withColumn("qty", col("count") / totalc1).drop("count").orderBy(desc("qty"))
            qt = qt.join(qty, Seq("_c2"), "full_outer")
            newNames = newNames++Seq("c"+i)
        }

        qt.toDF(newNames: _*).orderBy(desc("ref_id")).write.csv("qtyall")

       // 1.Confidence score needs DeNovo assembly of contigs from the reads
       // 2.alignment of contigs, BLAST (+ scoring and counting the aligments)


        spark.stop()
    }
}
