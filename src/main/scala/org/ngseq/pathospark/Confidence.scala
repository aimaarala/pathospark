package org.ngseq.pathospark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object Confidence {
    def main(args: Array[String]) = {

        Logger.getLogger("org").setLevel(Level.OFF)
        Logger.getLogger("akka").setLevel(Level.OFF)

        val spark = SparkSession.builder.master("local[*]").appName("main").config("spark.driver.memory", "5g").getOrCreate()


        val schemaString = "_c1 conf"
        val fields = schemaString.split(" ").map(fieldName => StructField(fieldName, StringType, nullable = true))
        val schema = StructType(fields)

        var confs = spark.read.option("sep", ",").schema(schema).option("inferSchema", "true").csv("blasted")
        // Generate the schema based on the string of schema
        var newNames = Seq("ref_id","c1")

        for (i <- 2 to 22) {
            var fn = "*.blast"

            val blast = spark.read.option("sep","\t").option("inferSchema", "true").csv(i+"/"+fn)

            val blasteddf = blast.orderBy(asc("_c10")).filter(col("_c11")>100).groupBy("_c1").count
            val separatecount = blast.groupBy("_c1").count
            val max = blasteddf.orderBy(desc("count")).first.getLong(1)
            val conf = blasteddf.withColumn("conf", col("count") / max).drop("count").orderBy(desc("conf"))

            confs = confs.join(conf, Seq("_c1"), "full_outer")
            newNames = newNames++Seq("c"+i)
        }

        confs.toDF(newNames: _*).orderBy(desc("ref_id")).write.csv("confsall")

       // 1.Confidence score needs DeNovo assembly of contigs from the reads
       // 2.alignment of contigs, BLAST (+ scoring and counting the aligments)


        spark.stop()
    }
}
