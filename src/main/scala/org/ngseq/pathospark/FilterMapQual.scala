package org.ngseq.pathospark

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object FilterMapQual {
    def main(args: Array[String]) = {

        Logger.getLogger("org").setLevel(Level.OFF)
        Logger.getLogger("akka").setLevel(Level.OFF)

        val spark = SparkSession.builder.master("local[*]").appName("main").config("spark.driver.memory", "5g").getOrCreate()

        // Generate the schema based on the string of schema

        for (i <- 1 to 21) {
            var fn = ""
            if(i>9){
                fn= "C" + i + "mapped.sam"
            }
            else {
                fn= "C0" + i + "mapped.sam"
            }
            val df = spark.read.option("sep", "\t").option("inferSchema", "true").csv("mapped/"+fn).drop(col("_c15")).withColumn("id", lit(1))
            val mqgt30 = df.filter(mq => mq.get(4).toString.toInt > 30)
            //val asf = mqgt30.withColumn("_c13", regexp_replace(col("_c13"), "AS:i:", "")).withColumn("_c13", '_c13.cast("Int"))
            //val as_filtered = asf.filter(v => v.get(13).toString.toInt > 50)
            //val totalc1 = as_filtered.count

            mqgt30.coalesce(1).write.partitionBy("_c2").option("sep", "\t").csv("filtered/"+i)
        }

        //qt.toDF(newNames: _*).orderBy(desc("ref_id")).write.csv("qtyall")

       // 1.Confidence score needs DeNovo assembly of contigs from the reads
       // 2.alignment of contigs, BLAST (+ scoring and counting the aligments)


        spark.stop()
    }
}
